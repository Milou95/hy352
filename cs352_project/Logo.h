#pragma once
#include "./hy352_gui.h"
#include "namespaces.h"


int tmp = 0;
list<int> mylist;

#define FALSE	0
#define TRUE	1


#define START_PROGRAM int main(int argc, char **argv) {init_GUI();
#define END_PROGRAM ; destroy_GUI();return 0;}

#define MAKE ;auto
#define ASSIGN ;

#define NUMBER  false ? 420					 //edw to suntaktiko akoloutheite apo colon , 
#define WORD    false ? "denexeishmasia"    // eite einai kollita to colon sth leksh eite oxi, apoth glwssa anagrorizontai 2 tokens panta
#define BOOLEAN false ? false              // to suntaktiko einai : ekfrash ? otanHekfrashTrue : otanHekfrashFalse 

#define AND and
#define OR or
#define NOT not

#define IF ;if(
#define ELIF ;}else if(
#define ELSE ;}else{

#define SENTENCE sent

#define FORWARD ;__hidden__::forward(),
#define BACK	;__hidden__::back(),
#define CIRCLE	;__hidden__::circle(),
#define CENTER	;turtle_go_to_center()

#define SETPENCOLOR		;set_pen_color
#define SETSCREENCOLOR	;set_screen_color
#define SETPENSIZE		;__hidden__::pensize(),
#define PENDOWN			;pen_down()
#define PENUP			;pen_up()

#define REPEAT		;while(
#define TIMES		>tmp 
#define WHILE       1&&
#define REPCOUNT	tmp++;
#define DO			){// cout<<tmp<<"\n"; if(tmp>=0)mylist.push_back(tmp);tmp=0;
#define END			;}//if( tmp!=0 ){ tmp = mylist.back(); mylist.pop_back(); }

#define LIST  mylist A;A
#define ARRAY (vector<boost::any>)mylist = 


float MINUS(double number) { return 0 - number; }
float QUOTIENT(float x, float y) { assert(y != 0); return x / y; }
int MODULO(int x, int y) { return x % y; }
bool not(bool s) { return s ? false : true; }
float DIFFERENCE(float x, float y) { return x - y; }

template <class T1, class T2, class ...Args> float SUM(const T1& first, const T2& second, const Args& ... args)
{
	float array[sizeof...(args)+2] = { first,second, args... };
	float sum = 0;
	for (unsigned n = 0; n < sizeof...(args)+2; n++) 	sum += array[n];
	return sum;
}
template <class T, class ...Args> float PRODUCT(const T& first, const T& second, const Args&... args)
{
	T array[sizeof...(args)+2] = { first,second, args... };
	float product = 1;
	for (unsigned n = 0; n < sizeof...(args)+2; n++) {
		product = array[n] * product;
	}

	return product;
}

template <class T, class ...Args> bool and(const T& first, const Args&... args)  // auto tha xreiastei sto AND(....)
{
	T array[sizeof...(args)+1] = { first, args... };
	int sum = 0;
	for (unsigned n = 0; n < sizeof...(args)+1; n++) {
		cout << n << "\n";
		if (array[n] == 1) {
			sum = sum + 1;
		}
	}
	return   (sum == sizeof...(args)+1) ? TRUE : FALSE;
}

template <class T, class ...Args> bool or (const T& first, const Args&... args)  // auto tha xreiastei sto AND(....)
{
	T arr[sizeof...(args)+1] = { first, args... };
	int sum = 0;
	for (unsigned n = 0; n < sizeof...(args)+1; n++) {
		cout << n << "\n";
		if (arr[n] == 1) {
			sum = sum + 1;
		}
	}
	return (sum == 0) ? FALSE : TRUE;
}



template <class string, class ...Args> list<string> sent(const string& first, const Args&... args) { // auto tha xreiastei sto AND(....)
	list<string> herelist;
	string arr[sizeof...(args)+1] = { first, args... };
	for (auto n : arr) herelist.push_back(n);	//cout << n << "\n";
	return herelist;
}
